# bridge.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
File Module
-----------

Python Design Patterns: Bridge

Decouple an abstraction from its implementation 
so that the two can vary independently. 

Bridge pattern has structural purpose and applies to objects, 
so it deals with the composition of objects.

When to use:
------------
You want to avoid a permanent binding between an abstraction 
and its implementation.
Both the abstractions and their implementations should be 
extensible by subclassing.
Changes in the implementation of an abstraction should have 
no impact on clients.
You want to hide the implementation of an abstraction completely 
from clients.
"""


class Implementor:
    """
    Implementor
    -----------
    defines the interface for implementation classes
    """

    def action(self) -> None:
        pass


class ConcreteImplementorA(Implementor):
    """
    ConcreteImplementorA
    --------------------
    Concrete Implementor

    implement the Implementor interface and define concrete implementations
    """

    def action(self) -> None:
        print("Concrete Implementor A")


class ConcreteImplementorB(Implementor):
    """
    ConcreteImplementorB
    --------------------
    Concrete Implementor

    implement the Implementor interface and define concrete implementations
    """

    def action(self) -> None:
        print("Concrete Implementor B")


class Bridge:
    """
    Bridge
    ------
    decouple an abstraction from its implementation
    """

    def __init__(self, implementation: Implementor) -> None:
        self._implementor = implementation

    def operation(self) -> None:
        self._implementor.action()


def run_bridge() -> None:
    bridge = Bridge(ConcreteImplementorA())
    bridge.operation()

    bridge = Bridge(ConcreteImplementorB())
    bridge.operation()


if __name__ == "__main__":
    run_bridge()
