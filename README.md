# pythonloveu
 Python Love you

## Python3 Build Dependencies
```
$ sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libgdbm-dev liblzma-dev libgdbm-compat-dev
```

## Install Python3x - Source - Override/Alongside
```
$ tar -xvf Python-3.x.x.tgz
$ cd Python-3.x.x
$ ./configure or ./configure --prefix=yourpath like /usr/local/python/3.x.x
$ make
$ make test
$ sudo make install/altinstall
```

## Envs
1. Create ENV
```
$ python3 -m venv my_env
```
2. Activate ENV
- Linux
```
$ source my_env/bin/activate
```
- Windows(Powershell)
```
cd my_env/Scripts
.\activate
```
3. Deactivate ENV 
```
$ deactivate
```
4. Add below config to your vscode workspace setting json file
```
"settings": {
    "python.venvPath": "your python envs container folder",
    "python.terminal.activateEnvironment": true,
    "python.pythonPath": "your python env python path",
}
```
5. Get your Python env Python path
```
$ source my_env/bin/activate
$ python
>>> import sys
>>> sys.executable
```

## QT5 Desinger
1. Install desinger
```
$ sudo apt-get install qttools5-dev-tools
$ sudo apt-get install qttools5-dev
```
2. Run
```
$ designer
```
3. Install dev tools
```
$ sudo apt install pyqt5-dev-tools
```
4. Convert .ui to .py, the command `pyuic5` is install by step 3
```
$ pyuic5 yourui.ui -o yourpy.py -x
```
5. Pip install pyqt5
```
pip install PyQt5
pip install PyQtWebEngine
```
