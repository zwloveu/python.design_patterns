# adapter.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
File Module
-----------

Python Design Patterns: Adapter

Convert the interface of a class into another interface 
the clients expect. 

Adapter lets classes work together that couldn't otherwise 
because of incompatible interfaces, ie. 
allows to use a client with an incompatible interface 
by an Adapter that does the conversion. 

Adapter has structural purpose and can be applied on classes 
and also on object. 

A class adapter uses multiple inheritance to adapt one interface 
to another and the object adapter uses object composition 
to combine classes with different interfaces.

When to use:
------------
You want to use an existing class, and its interface 
does not match the one you need.
You want to create a reusable class that cooperates 
with classes that don't necessarily have compatible interfaces.
"""

import sys


class Target:
    """
    Target
    ------
    defines specific interface that Client uses
    """

    def request(self):
        pass


class Adaptee:
    """
    Adaptee
    -------
    all requests get delegated to the Adaptee which defines
    an existing interface that needs adapting
    """

    def specificRequest(self) -> None:
        print("Specific request")


class Adapter(Target, Adaptee):
    """
    Adapter
    -------
    implements the Target interface and lets the Adaptee respond
    to request on a Target by extending both classes ie 
    adaptes the interface of Adaptee to the Target interface
    """

    def __init__(self) -> None:
        Adaptee.__init__(self)
        Target.__init__(self)

    def request(self) -> None:
        return self.specificRequest()


def run_adapter() -> None:
    t = Adapter()
    t.request()


if __name__ == "__main__":
    run_adapter()
