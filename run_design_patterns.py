# run_design_patterns.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

from abstract_factory import run_abstract_factory
from adapter import run_adapter
from bridge import run_bridge
from builder import run_builder
from chain_of_responsibility import run_chain_of_responsibility

if __name__ == "__main__":
    run_abstract_factory()
    print("-------------------------------------")
    run_adapter()
    print("-------------------------------------")
    run_bridge()
    print("-------------------------------------")
    run_builder()
    print("-------------------------------------")
    run_chain_of_responsibility()
    print("-------------------------------------")