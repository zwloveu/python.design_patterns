# abstract_factory.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
File Module
-----------

Python Design Patterns: Abstract Factory

Abstract factory pattern has creational purpose 
and provides an interface for creating families of related or dependent objects 
without specifying their concrete classes. 

Pattern applies to object and deal with object relationships, 
which are more dynamic. 

In contrast to Factory Method, Abstract Factory pattern produces family of types that are related, ie. 
it has more than one method of types it produces.

When to use:
------------
A system should be independent of how its products are created, composed, and represented.
A system should be configured with one of multiple families of products.
A family of related product objects is designed to be used together.
You want to provide a class library of products, and you want to reveal just their interfaces, 
not their implementations
"""

import sys


class ProductA:
    """
    ProductA
    --------
    products implment the same interface so that the classes can 
    refer to the interface not the concrete product
    """

    def get_name(self) -> None:
        pass


class ConcreteProductAX(ProductA):
    """
    ConcreteProductAX
    -----------------
    defines objects to be created by concrete factory
    """

    def get_name(self) -> str:
        return "A-X"


class ConcreteProductAY(ProductA):
    """
    ConcreteProductAY
    -----------------
    defines objects to be created by concrete factory
    """

    def get_name(self) -> str:
        return "A-Y"


class ProductB:
    """
    ProductB
    --------
    same as ProductA

    ProductB declares interface for concrete products
    where each can produce an entire set of products
    """

    def get_name(self) -> None:
        pass


class ConcreteProductBX(ProductB):
    """
    ConcreteProductBX
    -----------------
    defines objects to be created by concrete factory
    """

    def get_name(self) -> str:
        return "B-X"


class ConcreteProductBY(ProductB):
    """
    ConcreteProductBY
    -----------------
    defines objects to be created by concrete factory
    """

    def get_name(self) -> str:
        return "B-Y"


class AbstractFactory:
    """
    AbstractFactory
    ---------------
    provides an interface for creating a family of products
    """

    def create_producta(self) -> None:
        pass

    def create_productb(self) -> None:
        pass


class ConcreteFactoryX(AbstractFactory):
    """
    ConcreteFactoryX
    ----------------
    Concrete Factories

    each concrete factory create a family of products
    and client uses one of these factories
    """

    def create_producta(self) -> ConcreteProductAX:
        return ConcreteProductAX()

    def create_productb(self) -> ConcreteProductBX:
        return ConcreteProductBX()


class ConcreteFactoryY(AbstractFactory):
    """
    ConcreteFactoryY
    ----------------
    Concrete Factories

    each concrete factory create a family of products
    and client uses one of these factories
    """

    def create_producta(self) -> ConcreteProductAY:
        return ConcreteProductAY()

    def create_productb(self) -> ConcreteProductBY:
        return ConcreteProductBY()


def run_abstract_factory() -> None:
    factoryX = ConcreteFactoryX()
    factoryY = ConcreteFactoryY()

    p1 = factoryX.create_producta()
    print(f"Product: {p1.get_name()}")

    p2 = factoryY.create_producta()
    print(f"Product: {p2.get_name()}")


if __name__ == "__main__":
    run_abstract_factory()
