# builder.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
File Module
-----------

Python Design Patterns: Builder

Builder pattern has creational purpose and separates the construction 
of a complex object from its representation 
so that the same construction process can create different 
representations. 

It is object pattern, ie. relationships can be changed 
at run-time and are more dynamic. 

Often is used for building composite structures 
but constructing objects requires more domain knowledge 
of the client than using a Factory.

When to use:
------------
The algorithm for creating a object should be independent 
of the parts and how they're assembled.
The construction process must allow different representations 
for the object that's constructed.
"""


class Product:
    """
    Product
    -------
    the final object that will be created using Builder
    """

    def __init__(self) -> None:
        self._part_a = ""
        self._part_b = ""
        self._part_c = ""

    def make_a(self, part: str) -> None:
        self._part_a = part

    def make_b(self, part: str) -> None:
        self._part_b = part

    def make_c(self, part: str) -> None:
        self._part_c = part

    def get(self) -> str:
        return f"{self._part_a} {self._part_b} {self._part_c}"


class Builder:
    """
    Builder
    -------
    abstract interface fro creating products
    """

    def __init__(self) -> None:
        self._product: Product = Product()

    def get(self) -> Product:
        return self._product

    def build_part_a(self) -> None:
        pass

    def build_part_b(self) -> None:
        pass

    def build_part_c(self) -> None:
        pass


class ConcreteBuilderX(Builder):
    """
    ConcreteBuilderX
    ----------------
    Concrete Builder

    create real products and stores them in the composite structure
    """

    def __init__(self) -> None:
        Builder.__init__(self)

    def build_part_a(self) -> None:
        self._product.make_a("A-X")

    def build_part_b(self) -> None:
        self._product.make_b("B-X")

    def build_part_c(self) -> None:
        self._product.make_c("C-X")


class ConcreteBuilderY(Builder):
    """
    ConcreteBuilderX
    ----------------
    Concrete Builder

    create real products and stores them in the composite structure
    """

    def __init__(self) -> None:
        Builder.__init__(self)

    def build_part_a(self) -> None:
        self._product.make_a("A-Y")

    def build_part_b(self) -> None:
        self._product.make_b("B-Y")

    def build_part_c(self) -> None:
        self._product.make_c("C-Y")


class Director:
    """
    Director
    --------
    responsible for managing the correct sequenece of object creation
    """

    def __init__(self) -> None:
        self._builder: Builder = None

    def set(self, builder: Builder) -> None:
        self._builder = builder

    def get(self) -> Product:
        return self._builder.get()

    def construct(self) -> None:
        self._builder.build_part_a()
        self._builder.build_part_b()
        self._builder.build_part_c()


def run_builder() -> None:
    builderX = ConcreteBuilderX()
    builderY = ConcreteBuilderY()

    director = Director()
    
    director.set(builderX)
    director.construct()

    p1 = director.get()
    print(f"1st product parts: {p1.get()}")

    director.set(builderY)
    director.construct()

    p2 = director.get()
    print(f"2bd product parts: {p2.get()}")


if __name__ == "__main__":
    run_builder()
