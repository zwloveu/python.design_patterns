# chain_of_responsibility.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
File Module
-----------

Python Design Patterns: Chain of Responsibility

Chain of Responsibility pattern avoids coupling the sender 
of a request to its receiver by giving more than one object a chance 
to handle the request. 

The pattern chains the receiving objects and passes the request 
along the chain until an object handles it. 

It has a behavioral purpose and deals with relationships 
between objects.

When to use:
------------
More than one object may handle a request, 
and the handler should be ascertained automatically.
You want to issue a request to one of several objects 
without specifying the receiver explicitly.
The set of objects that can handle a request 
should be specified dynamically.
"""


class LeaveRequest:
    """
    LeaveRequest
    ------------
    defines a leave request with who and how long to leave
    """

    def __init__(self, who: str, howlong: int) -> None:
        self._who = who
        self._howlong = howlong

    def get_leave_days(self) -> int:
        return self._howlong

    def __repr__(self) -> str:
        return f"{self._who} raised a {self._howlong} days leave request"


class LeaveHandler:
    """
    Handler
    -------
    defines an interface for handing requests 
    and optionally implements the successor link
    """

    def __init__(self) -> None:
        self._handler = None
        self.days_leave_for_leader = 1
        self.days_leave_for_manager = 3
        self.days_leave_for_boss = 30

    def set_next_handler(self, handler) -> None:
        """
        Parameters
        ----------
        handler: is an implmentation for Handler
        """

        self._handler = handler

    def handle_leave_request(self, request: LeaveRequest) -> None:
        if not self._handler is None:
            self._handler.handle_leave_request(request)


class LeaderLeaveHandler(LeaveHandler):
    """
    LeaderLeaveHandler
    ------------------
    Concrete Handlers

    handle requests they are responsible for
    """

    def __init__(self, name) -> None:
        LeaveHandler.__init__(self)
        self._name = name

    def handle_leave_request(self, request: LeaveRequest) -> None:
        if request.get_leave_days() <= self.days_leave_for_leader:
            print(f"Handled by {self._name}")
        else:
            print(f"Cannot be handled by {self._name}")
            super().handle_leave_request(request)


class ManagerLeaveHandler(LeaveHandler):
    """
    ManagerLeaveHandler
    ------------------
    Concrete Handlers

    handle requests they are responsible for
    """

    def __init__(self, name) -> None:
        LeaveHandler.__init__(self)
        self._name = name

    def handle_leave_request(self, request: LeaveRequest) -> None:
        if request.get_leave_days() <= self.days_leave_for_manager:
            print(f"Handled by {self._name}")
        else:
            print(f"Cannot be handled by {self._name}")
            super().handle_leave_request(request)


class BossLeaveHandler(LeaveHandler):
    """
    ManagerLeaveHandler
    ------------------
    Concrete Handlers

    handle requests they are responsible for
    """

    def __init__(self, name) -> None:
        LeaveHandler.__init__(self)
        self._name = name

    def handle_leave_request(self, request: LeaveRequest) -> None:
        if request.get_leave_days() <= self.days_leave_for_boss:
            print(f"Handled by {self._name}")
        else:
            print(f"Refused by {self._name}")


def run_chain_of_responsibility() -> None:
    lr = LeaveRequest("Bob", 20)
    print(lr)

    leader_handler = LeaderLeaveHandler("Leader")
    manager_handler = ManagerLeaveHandler("Manager")
    boss_handler = BossLeaveHandler("Boss")

    leader_handler.set_next_handler(manager_handler)
    manager_handler.set_next_handler(boss_handler)

    leader_handler.handle_leave_request(lr)


if __name__ == "__main__":
    run_chain_of_responsibility()
